package sn.demo.myclassroom;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import sn.demo.myclassroom.domain.entities.Category;
import sn.demo.myclassroom.domain.service.CategoryRepository;
import sn.demo.myclassroom.web.controllers.MainController;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = MockServletContext.class)
//@WebAppConfiguration
//@WebMvcTest
class MyClassromApplicationTests {

//	@InjectMocks
//	MainController mainController;
//
//	@Mock
//	CategoryRepository categoryRepository;
//
//	private MockMvc mockMvc;
//
//	@Before
//	public void setup() {
//		MockitoAnnotations.initMocks(this);
//		mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
//
//	}
	
//	@Test
//	public void TestGetAllCategories() throws Exception {
//		when(categoryRepository.findAll()).thenReturn(new ArrayList<Category>());
//		mockMvc.perform(get("/categories"))
//		.andExpect(status().isOk())
//		.andExpect(content().string("[]"));
//	}
	
//	@Test
//	public void TestAddCategory() throws Exception {
//		when(categoryRepository.save(new Category(1L, "JAVA", null)))
//		.thenReturn(new Category(1L, "JAVA", null));
//		mockMvc.perform(post("/categories/add"))
//		.andExpect(status().isCreated())
//		.andExpect(content().string("http://localhost:8080/categories/add/1"));
//		verify(categoryRepository, times(2)).save(new Category(1L, "JAVA", null));
//	}

}
