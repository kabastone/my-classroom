package sn.demo.myclassroom.web.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import sn.demo.myclassroom.domain.entities.Category;
import sn.demo.myclassroom.domain.service.CategoryRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MockServletContext.class)
@WebMvcTest
public class MainControllerTests {

	@InjectMocks
	MainController mainController;

	@Mock
	CategoryRepository categoryRepository;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();

	}
	
	@Test
	public void TestGetAllCategories() throws Exception {
		when(categoryRepository.findAll()).thenReturn(new ArrayList<Category>());
		mockMvc.perform(get("/categories"))
		.andExpect(status().isOk())
		.andExpect(content().string("[]"));
	}
	
	@Test
	public void TestAddCategory() throws Exception {
		Category cat = new Category();
		cat.setId(1L);
		cat.setName("JAVA");
		cat.setCourses(null);
		when(categoryRepository.save(Mockito.any(Category.class)))
		.thenReturn(cat);
		mockMvc.perform(post("/categories/add")
				.content(asJsonString(cat))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isCreated())
		.andExpect(header().string("location", "http://localhost/categories/add/1"));
		verify(categoryRepository, times(1)).save(cat);
	}

	private static String asJsonString(final Category cat) {
		
		try {
			return new ObjectMapper().writeValueAsString(cat);
		} catch (JsonProcessingException e) {
			throw new RuntimeException();
		}
	}
}