package sn.demo.myclassroom.web.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyString;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import junit.framework.Assert;
import sn.demo.myclassroom.domain.entities.Course;
import sn.demo.myclassroom.domain.service.CourseRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MockServletContext.class)
@WebMvcTest
public class CourseControllerTests {
	@InjectMocks
	CourseController courseController;
	
	@Mock
	private CourseRepository courseRepository;
	
	private MockMvc mockMvc;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(courseController).build();
	}
	
	@Test
	public void getCourseByCatNameTest() throws Exception {
//		when(courseRepository.findByCategory_Name(anyString())).thenReturn(new ArrayList<Course>());
//		MvcResult mvcResult = mockMvc.perform(get("/courses/{catName}",  "catName"))
//				.andExpect(status().isOk())
//				.andExpect(content().string("[]"))
//				.andReturn();
//		        assertEquals("[]", mvcResult.getResponse().getContentAsString());
//		        
	}

}
