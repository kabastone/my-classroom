package sn.demo.myclassroom.domain.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import sn.demo.myclassroom.domain.entities.Course;

@CrossOrigin
public interface CourseRepository extends CrudRepository<Course, Long>{
	
}
