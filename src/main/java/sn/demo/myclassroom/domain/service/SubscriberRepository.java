package sn.demo.myclassroom.domain.service;

import org.springframework.data.repository.CrudRepository;

import sn.demo.myclassroom.domain.entities.Subscriber;

public interface SubscriberRepository extends CrudRepository<Subscriber, Long>{

}
