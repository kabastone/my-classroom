package sn.demo.myclassroom.domain.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sn.demo.myclassroom.domain.entities.Category;
@RepositoryRestResource
@CrossOrigin(origins = "*")
public interface CategoryRepository extends CrudRepository<Category, Long>    {
	@RestResource(path = "categoryNameStartWith", rel="categoryNameStartWith")
	public Category findCategoryByName(@Param(value = "name") String name);
}
