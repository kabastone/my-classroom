package sn.demo.myclassroom.domain.service;

import org.springframework.data.repository.CrudRepository;

import sn.demo.myclassroom.domain.entities.Section;

public interface SectionRepository extends CrudRepository<Section, Long>{

}
