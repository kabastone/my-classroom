package sn.demo.myclassroom.domain.service;

import org.springframework.data.repository.CrudRepository;

import sn.demo.myclassroom.domain.entities.User;

public interface UserRepository extends CrudRepository<User, Long>{
	public User findByUsername(String username);
}
