package sn.demo.myclassroom.domain.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import sn.demo.myclassroom.domain.entities.Author;
@RepositoryRestResource
@CrossOrigin(value = "*")
public interface AuthorRepository extends CrudRepository<Author, Long> {

}
