package sn.demo.myclassroom.domain.service;

import org.springframework.data.repository.CrudRepository;

import sn.demo.myclassroom.domain.entities.Lesson;

public interface LessonRepository extends CrudRepository<Lesson, Long> {

}
