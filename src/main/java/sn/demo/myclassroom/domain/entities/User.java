package sn.demo.myclassroom.domain.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name="USERS")
@Data
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "USER_ID")
	private Long id;
	
	@Column(name = "USERNAME")
	@NotEmpty
	private String username;
	
	@Column(name = "PASSWORD")
	@NotEmpty
	@JsonIgnore
	private String password;
	
	@Column(name = "FIRST_NAME")
	@NotEmpty
	private String firstName;
	
	@Column(name = "LAST_NAME")
	@NotEmpty
	private String lastName;
	
	@Column(name = "ADMIN", columnDefinition = "char(3)")
	@Type(type = "yes_no")
	@NotEmpty
	private boolean admin;
	
}
