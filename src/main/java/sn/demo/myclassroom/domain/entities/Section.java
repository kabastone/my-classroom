package sn.demo.myclassroom.domain.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Section implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String title;
	private boolean completed;
	@OneToMany(cascade = CascadeType.ALL)
	private Collection<Lesson> lessons;

	@Override
	public int hashCode() {
		return Objects.hash(completed, id, title, lessons);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Section))
			return false;
		Section other = (Section) obj;
		return completed == other.completed && Objects.equals(id, other.id) && Objects.equals(title, other.title)
				&& Objects.equals(lessons, other.lessons);
	}

	@Override
	public String toString() {
		return "Section [id=" + id + ", title=" + title + ", completed=" + completed + ", lessons=" + lessons + "]";
	}

}
