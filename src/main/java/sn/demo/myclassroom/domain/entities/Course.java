package sn.demo.myclassroom.domain.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Course implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String title;
	private String description;
	private String intro;
	private String roadMap;
	private String purpose;
	private String skills;
	private String img;
	private float rate;
	private String langage;
	private LocalDateTime updateDate;
	@OneToMany
	private Collection<Section> sections;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "author_id")
	private Author author;
	private Double price;
	private boolean isfree;
	private boolean isCompleted;
	@Enumerated(EnumType.STRING)
	private CourseStatus courseStatus;
	@ManyToMany
	private Collection<Note> notes;

}
