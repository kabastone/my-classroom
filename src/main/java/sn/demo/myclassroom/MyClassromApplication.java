package sn.demo.myclassroom;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import sn.demo.myclassroom.domain.entities.Author;
import sn.demo.myclassroom.domain.entities.Category;
import sn.demo.myclassroom.domain.entities.Course;
import sn.demo.myclassroom.domain.entities.CourseStatus;
import sn.demo.myclassroom.domain.entities.Lesson;
import sn.demo.myclassroom.domain.service.CategoryRepository;
import sn.demo.myclassroom.domain.service.CourseRepository;
import sn.demo.myclassroom.dto.LessonDTO;
import sn.demo.myclassroom.storage.StorageProperties;
import sn.demo.myclassroom.storage.StorageService;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class MyClassromApplication {

	@Autowired
	CategoryRepository categoryRepository;
	@Autowired
	CourseRepository courseRepository;
	
	private static final String MY_APP_URI = "http://localhost:8080/";
	private RestTemplate restTemplate = new RestTemplate();
	public static void main(String[] args) {
		SpringApplication.run(MyClassromApplication.class, args);
		
		
	}
	//@Override
	@Transactional
	public void run(String... args) throws Exception {
		Category cat1 = new Category();
		List<Course> courses = new ArrayList<>();
		Course course = new Course();
		course.setTitle("SpringBoot");
		course.setDescription("SpringBoot from Scratch");
		course.setIntro("");
		course.setRoadMap("");
		course.setImg("");
		course.setSkills("Rest service, data rest, maven");
		course.setPurpose("for backend developer");
		course.setSections(null);
		course.setPrice(99.99);
		//course.setCategory(cat1);
		course.setCourseStatus(CourseStatus.NEW);
		Author author = new Author(null, "Mike", "Dean", "deanmike@gmail.com");
		course.setAuthor(author);
		courses.add(course);
		//courses.add(course2);
		
		
		cat1.setName("JAVA8");
		
		cat1.setCourses(courses);
		//categoryRepository.save(cat1);
		Category cat2 = new Category();
		Course course2 = new Course();
		course2.setTitle("Django");
		course2.setDescription("RDjango framework");
		course2.setIntro("");
		course2.setRoadMap("");
		course2.setImg("");
		course2.setSkills("Redux, node");
		course2.setPurpose("for Backebd developer");
		course2.setSections(null);
		course2.setPrice(80.23);
		Author author2 = new Author(null, "Paul", "George", "george@gmail.com");
		course2.setAuthor(author2);
		//course2.setCategory(cat2);
		course2.setCourseStatus(CourseStatus.NEW);
		List<Course> pythonCourses = Arrays.asList(course2);
		cat2.setName("PYTHON");
		cat2.setCourses(pythonCourses);
		//URI locationUri1 = createCategory(cat1);
		//URI locationUri2 = createCategory(cat2);
		  cat1 = categoryRepository.save(cat1);
		  cat2 = categoryRepository.save(cat2);
		//System.out.println("URI nouvelle catégorie créée: " +cat1);
		//System.out.println("URI nouvelle catégorie créée: " +cat2);
		
		
		//System.out.println("BASIC HTTP AUTHENTIFICATION");
		//System.out.println("----------------------------------------------------------------");
		cat1 = getCategory(1L);
		//System.out.println(cat1);
		//System.out.println("Les cours de la catégorie " + cat1.getName() + ": " + cat1.getCourses() );
		//getCategories().forEach(System.out::println);

		// System.out.println(getCourseByCatName(1L));
		Lesson lesson = new Lesson();
		lesson.setTitle("HELLO WORLD");
		lesson.setDuration(Duration.ofMillis(20000));
		lesson.setId(null);
		lesson.setCodeSourceUrl("URL");
		System.out.println("--------------- Upload d'un fichier------------");
		//createLesson(lesson);
	}
	
	@Bean
	CommandLineRunner init(StorageService storageService) {
		return (args) -> {
			storageService.deleteAll();
			storageService.init();
			LessonDTO lesson = new LessonDTO();
			lesson.setTitle("HELLO WORLD");
			lesson.setDuration(Duration.ofMillis(20000));
			lesson.setId(1L);
			lesson.setCodeSourceUrl("URL");
			//lesson.setVideoUrl(getUserFileResource());
			//System.out.println("--------------- Upload d'un fichier------------");
			//createLesson(lesson);
		};
	}
	
	public void createLesson(LessonDTO lesson) throws IOException {
		MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
	      bodyMap.add("lessonFile", getUserFileResource());
	      bodyMap.add("entity", lesson);
	      
	      HttpHeaders headers = new HttpHeaders();
	      headers.setContentType(MediaType.MULTIPART_MIXED);
	      HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

	      RestTemplate restTemplate = new RestTemplate();
	      /*ResponseEntity<String> response = restTemplate.exchange(MY_APP_URI + "lessons/new",
	              HttpMethod.POST, requestEntity, String.class);
	              */
	      URI uri = restTemplate.postForLocation(MY_APP_URI + "lessons/new", requestEntity);
	      //System.out.println("response status: " + response.getStatusCode());
	      //System.out.println("response body: " + response.getBody().toString());
	      System.out.println(uri);
	}
	
	private Resource getUserFileResource() throws IOException {
		 Path tempFile = Files.createTempFile("upload-test-file", ".txt");
	      Files.write(tempFile, "some test content...\nline1\nline2".getBytes());
	      System.out.println("uploading: " + tempFile);
	      File file = tempFile.toFile();
	      //to upload in-memory bytes use ByteArrayResource instead
	      return new FileSystemResource(file);
		
	}
	public URI createCategory(Category category) {
		return restTemplate.postForLocation(MY_APP_URI + "categories", category);
	}
	
	public URI createCourse(Course course) {
		return restTemplate.postForLocation(MY_APP_URI + "courses", course);
	}
	
	public List<Category> getCategories() {
		ParameterizedTypeReference<List<Category>> responseType = new ParameterizedTypeReference<List<Category>>() {
		};
		
		ResponseEntity<List<Category>> response = restTemplate.exchange(MY_APP_URI + "categories", HttpMethod.GET, null, responseType);
		List<Category> allCategories = response.getBody(); 
		return allCategories;
		
	}
	
	private HttpHeaders getAuthentificationHeader(String username, String password) {
		String credentials = username + ":" + password;
		byte[] base64CredentialData = Base64.encodeBase64(credentials.getBytes());
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Basic " + new String(base64CredentialData));
		
		return headers;
	}
	
	public Category getCategory(Long id) {
		HttpHeaders authentifcationHeaders = getAuthentificationHeader("admin", "admin");
		ResponseEntity<Category> cat = restTemplate.exchange(MY_APP_URI + "categories/{id}", HttpMethod.GET, new HttpEntity<Void>(authentifcationHeaders), Category.class, id);
	  return cat.getBody();
	}
//	public String getCourseByCatName(Long id) {
//		ParameterizedTypeReference<List<Course>> responseType = new ParameterizedTypeReference<List<Course>>() {
//		};
//		UriBuilder uri = UriComponentsBuilder.newInstance()
//				.scheme("http")
//				.host("localhost").port("8080")
//				.path("courses/search/categoryId")
//				.queryParam("id", id);
//		ResponseEntity<String> response = restTemplate.exchange(uri.build().toString(), HttpMethod.GET, null, String.class);
//	
//		return response.getBody();
	//}
}
