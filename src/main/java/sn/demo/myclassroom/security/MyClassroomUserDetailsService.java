package sn.demo.myclassroom.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import sn.demo.myclassroom.domain.entities.User;
import sn.demo.myclassroom.domain.service.UserRepository;

@Component
public class MyClassroomUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = Optional.ofNullable(userRepository.findByUsername(username))
				             .orElseThrow(() -> {
				            	throw new UsernameNotFoundException
				            	(String.format("User with the username %s doesn't exist", username));
				            		});
		
		// create a granted authority based on user's role
		// Can't pass null authority tu user. Hence initialize with an empty arraylist.
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		if(user.isAdmin()) {
			authorities = AuthorityUtils.createAuthorityList("ROLE_ADMIN");
		}
		// Create a UserDetails object from data
		UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
		
		return userDetails;
	}

}
