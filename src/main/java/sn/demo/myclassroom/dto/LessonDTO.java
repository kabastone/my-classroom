package sn.demo.myclassroom.dto;

import java.io.InputStream;
import java.time.Duration;
import java.util.Objects;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public class LessonDTO {
	private Long id;
	private String title;
	private MultipartFile videoUrl;
	private String codeSourceUrl;
	private Duration duration;
	
	public LessonDTO() {
	}
	
	public LessonDTO(Long id, String title, MultipartFile videoUrl, String codeSourceUrl, Duration duration) {
		super();
		this.id = id;
		this.title = title;
		this.videoUrl = videoUrl;
		this.codeSourceUrl = codeSourceUrl;
		this.duration = duration;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public MultipartFile getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(MultipartFile inputStream) {
		this.videoUrl = inputStream;
	}
	public String getCodeSourceUrl() {
		return codeSourceUrl;
	}
	public void setCodeSourceUrl(String codeSourceUrl) {
		this.codeSourceUrl = codeSourceUrl;
	}
	public Duration getDuration() {
		return duration;
	}
	public void setDuration(Duration duration) {
		this.duration = duration;
	}
	@Override
	public int hashCode() {
		return Objects.hash(codeSourceUrl, duration, id, title);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof LessonDTO))
			return false;
		LessonDTO other = (LessonDTO) obj;
		return Objects.equals(codeSourceUrl, other.codeSourceUrl) && Objects.equals(duration, other.duration)
				&& Objects.equals(id, other.id) && Objects.equals(title, other.title);
	}
	@Override
	public String toString() {
		return "LessonDTO [id=" + id + ", title=" + title + ", videoUrl=" + videoUrl + ", codeSourceUrl="
				+ codeSourceUrl + ", duration=" + duration + "]";
	}
	
}
