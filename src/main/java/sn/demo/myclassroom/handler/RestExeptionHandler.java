package sn.demo.myclassroom.handler;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import sn.demo.myclassroom.domain.dto.error.ErrorDetail;
import sn.demo.myclassroom.domain.dto.error.ValidationError;
import sn.demo.myclassroom.exeption.ResourceNotFoundException;

@ControllerAdvice
public class RestExeptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	private MessageSource messageSource;
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException rnfe, HttpServletRequest request) {
		ErrorDetail errorDetail = new ErrorDetail();
		errorDetail.setTimeStamp(Instant.now().toEpochMilli());
		errorDetail.setStatus(HttpStatus.NOT_FOUND.value());
		errorDetail.setTitle("Ressource Not Found");
		errorDetail.setDetail(rnfe.getMessage());
		errorDetail.setDeveloperMessage(rnfe.getClass().getName());
		return new ResponseEntity<>(errorDetail, null, HttpStatus.NOT_FOUND);
	}
	
//	@ExceptionHandler(MethodArgumentNotValidException.class)
//	@ResponseStatus(HttpStatus.BAD_REQUEST)
//	public @ResponseBody ErrorDetail handleValidationError(MethodArgumentNotValidException manve,
//			 HttpServletRequest request){
//		ErrorDetail errorDetail = new ErrorDetail();
//		errorDetail.setTimeStamp(Instant.now().toEpochMilli());
//		errorDetail.setStatus(HttpStatus.BAD_REQUEST.value());
//		String requestPath = (String) request.getAttribute("javax.servlet.error.request_uri");
//		if(requestPath == null) {
//			requestPath = request.getRequestURI();
//		}
//		
//		errorDetail.setTitle("Validation Failed");
//		errorDetail.setDetail("Input Validaion failed");
//		errorDetail.setDeveloperMessage(manve.getClass().getName());
//		
//		List<FieldError> fieldErrors = manve.getBindingResult().getFieldErrors();
////		for(FieldError fe : fieldErrors) {
////			List<ValidationError> validationErrorList = errorDetail.getErrors()
////					.get(fe.getField());
////			if(validationErrorList == null) {
////				validationErrorList = new ArrayList<ValidationError>();
////				errorDetail.getErrors().put(fe.getField(), validationErrorList);
////			}
////			ValidationError validationError = new ValidationError();
////			validationError.setCode(fe.getCode());
////			validationError.setMessage(fe.getDefaultMessage());
////		    validationErrorList.add(validationError);
////		}
//		
//		fieldErrors.forEach(fe -> {
//			List<ValidationError> validationErrorList =  Optional.ofNullable(errorDetail.getErrors()
//                    .get(fe.getField()))
//                    .map(Collection::stream)
//                    .orElseGet(Stream::empty)
//                    .collect(Collectors.toList());
//		  ValidationError validationError = new ValidationError();
//      	  validationError.setCode(fe.getCode());
//      	  validationError.setMessage(messageSource.getMessage(fe, null));
//      	  validationErrorList.add(validationError);
//			errorDetail.getErrors().put(fe.getField(), validationErrorList);
//		});
//
//		return errorDetail;
//		
//	}
//
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorDetail errorDetail = new ErrorDetail();
		errorDetail.setTimeStamp(Instant.now().toEpochMilli());
		errorDetail.setStatus(status.value());
		errorDetail.setTitle("Message Not Readable");
		errorDetail.setDetail(ex.getMessage());
		errorDetail.setDeveloperMessage(ex.getClass().getName());
		
		return handleExceptionInternal(ex, errorDetail, headers, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException manve,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorDetail errorDetail = new ErrorDetail();
		errorDetail.setTimeStamp(Instant.now().toEpochMilli());
		errorDetail.setStatus(HttpStatus.BAD_REQUEST.value());
		errorDetail.setTitle("Validation Failed");
		errorDetail.setDetail("Input Validaion failed");
		errorDetail.setDeveloperMessage(manve.getClass().getName());
		List<FieldError> fieldErrors = manve.getBindingResult().getFieldErrors();
		fieldErrors.forEach(fe -> {
			List<ValidationError> validationErrorList =  Optional.ofNullable(errorDetail.getErrors()
                    .get(fe.getField()))
                    .map(Collection::stream)
                    .orElseGet(Stream::empty)
                    .collect(Collectors.toList());
		  ValidationError validationError = new ValidationError();
      	  validationError.setCode(fe.getCode());
      	  validationError.setMessage(messageSource.getMessage(fe, null));
      	  validationErrorList.add(validationError);
			errorDetail.getErrors().put(fe.getField(), validationErrorList);
		});
		
		return handleExceptionInternal(manve, errorDetail, headers, status, request);
	}

	
}
