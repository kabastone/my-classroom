package sn.demo.myclassroom.web.controllers;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import sn.demo.myclassroom.domain.entities.Lesson;
import sn.demo.myclassroom.domain.service.LessonRepository;
import sn.demo.myclassroom.dto.LessonDTO;
import sn.demo.myclassroom.storage.StorageService;


@RestController
@ControllerAdvice
@CrossOrigin("http://localhost:4200")
public class LessonController {
	@Autowired
	LessonRepository lessonRepository;
	
	private final StorageService storageService;
	
	@Autowired
	public LessonController(StorageService storageService) {
		this.storageService = storageService;
	}
	
	@PostMapping("/lessons/new")
	public ResponseEntity<String> saveLesson(@ModelAttribute LessonDTO lessonDTO ) {
		    Lesson lesson = new Lesson();
		    lesson.setCodeSourceUrl(lessonDTO.getCodeSourceUrl());
		    lesson.setDuration(lessonDTO.getDuration());
		    lesson.setTitle(lessonDTO.getTitle());
		    
		    MultipartFile file = (MultipartFile) lessonDTO.getVideoUrl();
			storageService.store(file);
			lesson.setVideoUrl(file.getOriginalFilename());
			Lesson less = lessonRepository.save(lesson);
			HttpHeaders headers = new HttpHeaders();
			URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(less.getId())
					.toUri();
			headers.setLocation(uri);
			return new ResponseEntity<>(null, headers, HttpStatus.CREATED);
			
	}

	@GetMapping("/files/{filename:.+}")
	public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

		Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}
	

}
