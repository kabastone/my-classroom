package sn.demo.myclassroom.web.controllers;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import sn.demo.myclassroom.domain.entities.Category;
import sn.demo.myclassroom.domain.service.CategoryRepository;
import sn.demo.myclassroom.exeption.ResourceNotFoundException;

//@RestController("myClassroomControllerV2")
//@RequestMapping({"/", "/oauth2/v2/"})
//@CrossOrigin
public class MainController {
	
//	@Autowired
//	CategoryRepository categoryRepository;
//	
//	public void verify(Long categoryId) {
//		Optional<Category> cat = categoryRepository.findById(categoryId);
//		  if (!cat.isPresent()) {
//			  throw new ResourceNotFoundException("Category id " + categoryId + " not found.");
//		  }
//		
//	
//	}
//	
//	@PostMapping("/categories/add")
//	public ResponseEntity<?> addCategory(@Valid @RequestBody Category category) {
//		Category cat = categoryRepository.save(category);
//		if(cat == null) {
//			throw new ResourceNotFoundException("Une erreur est survenue");
//		}
//		HttpHeaders headers = new HttpHeaders();
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
//				.path("/{id}")
//				.buildAndExpand(cat.getId())
//				.toUri();
//		headers.setLocation(uri);
//		return new ResponseEntity<>(null, headers, HttpStatus.CREATED);
//	}
//	
//	@GetMapping("/categories") 
//	public ResponseEntity<Iterable<Category>> getAllCategories(){
//		Iterable<Category> allCat = categoryRepository.findAll();
//		return new ResponseEntity<>(allCat, HttpStatus.OK);
//	}
//	
//	//@PreAuthorize("hasAuthority('ROLE_ADMIN')")
//	@GetMapping("/categories/{id}")
//	public ResponseEntity<Category> getCategory(@PathVariable Long id) {
//		verify(id);
//		Optional<Category> category = categoryRepository.findById(id);
//		
//		return new ResponseEntity<>(category.get(), HttpStatus.OK);
//	}
}
