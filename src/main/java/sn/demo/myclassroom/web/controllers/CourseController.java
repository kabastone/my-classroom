package sn.demo.myclassroom.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sn.demo.myclassroom.domain.entities.Course;
import sn.demo.myclassroom.domain.service.CourseRepository;

@RestController
@RequestMapping({"/", "/oauth/v2/"})
@CrossOrigin
public class CourseController {
	
	@Autowired
	private CourseRepository courseRepository;
	
//	//@GetMapping("/courses/{catName}")
//	public ResponseEntity<List<Course>> getCoursesByCatName(@PathVariable String catName) {
////		List<Course> courseList = Optional.ofNullable(courseRepository.findByCategoryName(catName))
////												.orElseGet(ArrayList<Course>::new);
//		List<Course> courseList = courseRepository.findByCategory_Name(catName);
//		return new ResponseEntity<>(courseList, HttpStatus.OK);
//	}

}
